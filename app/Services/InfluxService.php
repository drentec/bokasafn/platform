<?php


namespace App\Services;


use InfluxDB\Client;
use InfluxDB\Database;

class InfluxService
{
    /**
     * @var Client
     */
    private $influxClient;

    /**
     * @var \InfluxDB\Database
     */
    private $database;

    /**
     * InfluxService constructor.
     * @param Client $influxClient
     */
    public function __construct(Client $influxClient)
    {
        $this->influxClient = $influxClient;
        $this->database = $this->influxClient->selectDB(env("INFLUX_DATABASE"));
    }

    public function writePointData(array $points): bool
    {
        $result = $this->database->writePoints($points, Database::PRECISION_SECONDS);
        return $result;
    }
}
