<?php

namespace App\Http\Middleware;

use App\Services\InfluxService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use InfluxDB\Client;
use InfluxDB\Database;
use InfluxDB\Exception;
use InfluxDB\Point;

class InfluxLogger
{
    /**
     * @var InfluxService
     */
    private $influxService;

    /**
     * InfluxLogger constructor.
     * @param InfluxService $influxService
     */
    public function __construct(InfluxService $influxService)
    {
        $this->influxService = $influxService;
    }


    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws Database\Exception
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);


        $tags = [
            'path' => $request->path(),
            'user_agent' => $request->userAgent(),
            'ip' => explode(".", $request->ip())[0],
            'method' => $request->method(),
            'status' => $response->getStatusCode()
        ];

        if($request->get("tracking") != NULL) {
            $tags['tracking'] = $request->get("tracking");
        }
        if($request->server("HTTP_REFERER") != NULL) {
            $tags['referer'] = $request->server("HTTP_REFERER");
        }

        $points = [
            new Point('platform.request', 1, $tags)
        ];

        $this->influxService->writePointData($points);

        return $response;
    }
}
